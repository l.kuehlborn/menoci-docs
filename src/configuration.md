# Configuration

After installing Drupal and the **menoci** modules, some basic settings 
have to be configured through the Drupal administration user interface.

_Configuration instructions on this page are currently work in progress._

### Modules

Navigate your browser to the route `admin/modules/` of your Drupal instance.

1. Enable "jQuery Update" module

### Appearance

Go to `admin/appearance/`

1. Enable "Bootstrap" theme and set as default.

2. [optional] In Bootstrap settings, "Advanced": set "CDN provider" 
to "custom" and replace default paths 
(like `https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/*`) with  
`/sites/all/libraries/bootstrap`. 
The result should look like this 
for the the first CSS parameter:  
`/sites/all/libraries/bootstrap/css/bootstrap.css`   
respectively for the CSS and JS fields. (This is optional but Bootstrap 
library files are shipped  with the default Docker container, allowing 
you to minimize CDN traffic and related user tracking.)

![Bootstrap theme configuration](img/bootstrapconfig.png)

### Modules

Go to `admin/modules/`

1. Enable "Commons" module, press "save" button, and confirm that module 
dependencies will be automatically installed.
2. Enable "Published Data Registry" module.
3. Choose from the following optional **menoci** modules to install:
    * Mouseline Catalogue
    * Research Data Archive
    * Wikidata extension

##### Configuration

Go to `admin/config/`

1. Create first research group
2. [optional] Create subprojects

---


[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")](http://creativecommons.org/licenses/by-sa/4.0/)
