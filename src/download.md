
## Download

Get the latest version from our public distribution [GitLab Repository](https://gitlab.gwdg.de/medinfpub/menoci/). 

#### Releases

The following official distribution release versions of the menoci 
project are available: 

| Version | Date | DOI | Comment |
| --- | --- | --- | --- |
| 1.0 | 2020-02-05 | [doi:10.25625/NC9TF6](https://doi.org/10.25625/NC9TF6) | Initial menoci release |

---


[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")](http://creativecommons.org/licenses/by-sa/4.0/)
