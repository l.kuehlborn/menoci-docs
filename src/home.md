<center><img src="img/menoci_logo.png" alt="menoci logo" style="margin: 20px;"></center>

# Welcome to menoci.io

**menoci** is a collection of **open source** Drupal extension modules 
that can be used to rapidly deploy an **integrated website 
and data management portal** for research projects in the biomedical 
and life sciences.

The menoci modules have are actively developed and maintained by the 
University Medical Center Göttingen, 
[Department of Medical Informatics](http://mi.umg.eu), and Scientific 
Data Center Göttingen, [GWDG](https://gwdg.de).

### Architecture

Based on the [Drupal](https://drupal.org) web content management 
system, **menoci** introduces multiple 
extension modules that support scientific data management for life 
science research projects.

[![Schematic representation of the menoci system architecture.](img/architecture.png "Schematic representation of the menoci system architecture")](img/architecture.png)

### Features

Features of **menoci** include:

* Manage and display the scientific track record of your project in a 
standardized format (peer-reviewed article, published datasets, 
preprints, etc.)
* Document and display all resources linked to the published data
* Keep track of key assets for your biomedical experiments:
  * **Antibodies**,
  * **Mouse lines** and mice,
  * **Cell lines**\* and cell models (iPSC, hiPSC)
  * **Laboratory notebooks**\*
* Store your research data in [CDSTAR](https://cdstar.gwdg.de) and publish datasets with persistent 
resolvable identifiers from the 
[ePIC consortium](https://pidconsortium.net).

_\*) The menoci modules "Cell Model Catalogue" and "Lab Notebook Registry" 
are not currently included in the default software distribution but 
are freely available upon request._ 

## Reference projects

| Project | Description | Duration |
| ------- | ----------- | -------- |
| [CRC 1002](https://sfb1002.med.uni-goettingen.de) | The project investigates heart insufficiency in translational medicine. Origin of the menoci software. | 2012 - *today* |
| [CRC 1190](https://sfb1190.med.uni-goettingen.de) | Compartmental Gates and Contact Sites in Cells. _[more information](https://www.sfb1190.de/)_ | 2014 - *today* |
| [CRC 1286](https://sfb1286.uni-goettingen.de) | The project is specialised in Quantitative Synaptology and promotes a modern infrastructure including super-resolution imaging facilities. _[more information](https://www.sfb1286.de/)_ | 2017 - *today* |

## Literature

The menoci software project has been covered in the 
following scientific articles:

* Suhr M, Lehmann C, Bauer CR, Bender T, Knopp C, Freckmann L, et al. _menoci: Lightweight 
Extensible Web Portal enabling FAIR Data Management for Biomedical Research Projects_. 
 Feb 2020, [arXiv:200206161](https://arxiv.org/abs/2002.06161). **PREPRINT**
* Lehmann C, Suhr M, Umbach N, Cyganek L, Kleinsorge M, Nussbeck SY, et al. 
_Leaving spreadsheets behind – FAIR documentation and representation of 
human stem cell lines in the Collaborative Research Centre 1002_. 
GMDS 2019, Dortmund. [doi:10.3205/19gmds037](https://doi.org/10.3205/19gmds037).
* Rheinländer S, Aschenbrandt G, Nussbeck SY, Suhr M, Kusch H. 
_Towards standardized documentation of mouse lines in biomedical basic 
research_. GMDS 2019, Dortmund. [doi:10.3205/19gmds038](https://doi.org/10.3205/19gmds038).
* Suhr M, Jahn N, Mietchen D, Kusch H. _Wikidata as semantic 
representation platform of the scientific achievements of the 
biomedical Collaborative Research Centre 1002_.
 GMDS 2018, Osnabrück. [doi:10.3205/18gmds173](https://doi.org/10.3205/18gmds173).
* Kusch H, Schmitt O, Marzec B, Nussbeck SY. _Data organization of 
a clinical Collaborative Research Center in an integrated, 
long-term accessible Research Data Platform_. GMDS 2015, Krefeld. 
[doi:10.3205/15gmds104](https://doi.org/10.3205/15gmds104).
 
## Contact

This project is currently maintained by 
* [Luca Freckmann](https://orcid.org/0000-0002-8285-2586), 
luca.freckmann@med.uni-goettingen.de, 
University Medical Center Göttingen, [Department of Medical Informatics](http://mi.umg.eu)
* [Markus Suhr](https://orcid.org/0000-0002-6307-3253), 
markus.suhr@med.uni-goettingen.de, 
University Medical Center Göttingen, [Department of Medical Informatics](http://mi.umg.eu)

---


[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")](http://creativecommons.org/licenses/by-sa/4.0/)
