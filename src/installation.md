# Installation

On any Linux-based host system with _docker_ and _docker-compose_ 
packages installed: Get the latest **menoci** release version, 
for example by cloning the official Gitlab repository:

* `git clone https://gitlab.gwdg.de/medinfpub/menoci.git`
* `cd menoci` to enter the new directory

For a quick exploration, build a Docker image from the shipped [Dockerfile](https://gitlab.gwdg.de/medinfpub/menoci/blob/master/Dockerfile):
* `docker build -t local/menoci .`
* `docker run -p 80:80 --name menoci local/menoci`
* Open `localhost` in web browser, proceed with Drupal installation 
(choose *SQLite* database since no other database is available for 
this container)

More permanent installation should be done via `docker-compose.yml` with command `docker-compose up -d`:  
preferably to path /opt/docker/menoci

```yaml
version: '2.0'

services:

  drupal:
    build: . # Build local image
    ports:
      - 80:80
    volumes:
      - sites:/var/www/html/sites

  database:
    image: mariadb
    volumes:
      - mysql:/var/lib/mysql
    environment:
      MYSQL_RANDOM_ROOT_PASSWORD: "yes"
      MYSQL_DATABASE: drupal
      MYSQL_USER: menoci
      MYSQL_PASSWORD: ChangeIt

volumes:
  sites:
  mysql:
```

When using `docker-compose.yml` file to orchestrate webserver and database 
containers, be sure to correctly map the database service name from the compose file 
to database hostname during Drupal installation:

![Database configuration](img/dbconfig.png)

## Database choice

* Installation with MySQL/MariaDB as a database server is recommenced.
* SQLite may be a valid choice for testing purposes but some Drupal 
database mapping may diverge between engines.
* **Only MySQL/MariaDB is tested and validated by the development team.**

## Reverse Proxy HTTPS Settings

If you operate the menoci container behind a reverse proxy that adds HTTPS support, 
additional configuration is required to tell the Drupal instance about its base URL:

* First you need to fetch the `settings.php` from the running Drupal/menoci Docker container:  
  `docker cp drupal_container:/var/www/html/sites/default/settings.php /local/path` 
  (paths may differ in your installation...)
* Edit `settings.php`: Set the variable `$base_url` to your full URL including `https://` protocol
* Re-transfer the edited file into the container file system:  
  `docker cp /local/path/settings.php drupal_container:/var/www/html/sites/default/settings.php`
* Restart the container, `docker restart drupal_container`
* if settings.php is not writable [permissions](https://www.drupal.org/docs/7/install/step-3-create-settingsphp-and-the-files-directory#s-step-2-check-the-permissions-are-writeable) need to be changed
  * `chmod 644 sites/default/settings.php `

---




[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")](http://creativecommons.org/licenses/by-sa/4.0/)
