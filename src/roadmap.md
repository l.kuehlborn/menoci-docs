# Roadmap

Development of _menoci_ is actively pursued at the Göttingen Department of 
Medical Informatics. Please find below a rough outline of future release versions and 
features to expect.

The _menoci_ project roadmap is subject to change. If you would like to see certain 
features implemented on schedule, feel free to 
[contact us and contribute to development](https://gitlab.gwdg.de/medinfpub/menoci/#contribute).

### Version 1.1, _June 2020_

* **Node Link Extension**: a new prototype module will be included that enables 
integration of configurable Drupal content types into the _menoci_ Published Data 
Registry. Arbitrary data types can be integrated as additional "Catalogues" without 
the need to write actual source code. 

### Version 1.2, _July 2020_

* **Antibody Catalogue**: 
  * JSON-LD Support: Extending JSON to JSON-LD to enhance linked data functionality.
  * Automated PID Generation: Migrate from pre-registered PIDs to automated generation of PIDs.
* **Mouseline Catalogue**: 
  * JSON-LD Support: Extending JSON to JSON-LD to enhance linked data functionality.
  * Automated PID Generation: Migrate from pre-registered PIDs to automated generation of PIDs.
  * Traceability: Add features to track creators and editors of Mouseline Catalogue contents.

### Later releases, _tdb._

* **Advanced Light Microscopy and Nanoscopy (ALMN)**: The ALMN module provides workflow 
support and data management functionality for imaging facilities. The public release 
version will feature fully configurable project branding options.
  * Initial publication of generic module version
* **Lab Notebook registry**: The Lab Notebook Registry provides users with the option 
to create a digital representation of their orginal paper notebooks used for primary 
experimental documentation at the bench.
  * Automated PID Generation: Migrate from pre-registered PIDs to automated generation of PIDs
* **Cell Model Catalogue**: 
  * Initial publication of generic module version
  * JSON-LD Support: Extending JSON to JSON-LD to enhance linked data functionality.
* **Drupal Major Version Upgrade**: Migration from Drupal 7 to a recent 
release version.
* **Dissemination and Development**: First steps are already taken to further disseminate 
menoci among the scientific community and to spread the development among multiple stakeholders.
  * Extend development processes to other stakeholders
* **Dataverse Integration**: Allow upload of research data into Dataverse for 
long-term preservation and DOI registration.

---

[![Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "Creative Commons Attribution-ShareAlike 4.0 International License")](http://creativecommons.org/licenses/by-sa/4.0/)
